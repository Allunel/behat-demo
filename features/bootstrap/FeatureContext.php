<?php

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Hook\Scope\BeforeStepScope;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

// use Behat\Behat\Context\ClosuredContextInterface;
// use Behat\Behat\Context\TranslatedContextInterface;
// use Behat\Behat\Context\BehatContext;
// use Behat\Behat\Exception\PendingException;

use Behat\MinkExtension\Context\MinkContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {


  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /**
  * @When I wait for :seconds sec
  */
  public function iWaitForSec($seconds)
  {
    $this->getSession()->wait($seconds*1000);
  }




}
