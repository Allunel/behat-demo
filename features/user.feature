Feature: User can learn
  In order to use webcourses
  As a oficial eclectic pupil
  I need to be able to use coures as logged in user
  And when I've pay for them

  Scenario: Anonymouse user can't use courses
    Given I am an anonymous user
    And I am on homepage
    Then I should not see the link "Courses"

  @api
  Scenario: Eclectic pupil can use courses
    Given I am logged in as a user with the "pupil" role
    And I am on homepage
    When I click "Courses"
    Then I should see the heading "Welcome to courses"

  @javascript
  Scenario: Fade out heading title
    Given I am on the homepage
    When I fill in "search_block_form" with "a"
    And I wait for 2 sec
    Then I should not see the heading "Welcome to Webcourses"
